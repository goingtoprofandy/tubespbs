package com.aitekteam.developer.tubespbs;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    private DatabaseReference mDatabase;
    public Button button_play;
    private Integer LEDStatus;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        LEDStatus = 0;
        mDatabase = FirebaseDatabase.getInstance().getReference().child("LEDStatus");
        mDatabase.addValueEventListener(valueEventListener);
        button_play = findViewById(R.id.button_play);
        button_play.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.button_play:
                if (LEDStatus == 0) LEDStatus = 1;
                else LEDStatus = 0;
                mDatabase.setValue(LEDStatus);
                Log.d("LEDStatus", "" + LEDStatus);
                break;
        }
    }

    private ValueEventListener valueEventListener = new ValueEventListener() {
        @Override
        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
            LEDStatus = dataSnapshot.getValue(Integer.class);
            if (LEDStatus.intValue() == 1) {
                button_play.setText("ON");
            }
            else button_play.setText("OFF");
        }

        @Override
        public void onCancelled(@NonNull DatabaseError databaseError) {}
    };
}
